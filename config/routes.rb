Rails.application.routes.draw do
  root "initiative#index"

  get "/testando", to: "initiative#testando"

  namespace :api do
    namespace :v1 do
      get '/creatures/:id/weapons', to: 'creatures#weapons'
    end
  end

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'
end
