class InitiativeChannel < ApplicationCable::Channel
  def subscribed
    stream_from "initiative"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def add_dice
  end
end
