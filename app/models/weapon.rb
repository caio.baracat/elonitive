class Weapon < ApplicationRecord

  has_many :attacks
  has_many :creatures, through: :attacks

end
