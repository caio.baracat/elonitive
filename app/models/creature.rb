class Creature < ApplicationRecord

  has_many :attacks
  has_many :weapons, through: :attacks

  def self.calculate_dex_mod dex
    return ((dex - 10) / 2).floor
  end

end
