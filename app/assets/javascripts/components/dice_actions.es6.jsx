const DiceActions = React.createClass({
  render () {
    const actionButtonList = []
    for (const [i, a] of this.props.actions.entries()) {
      actionButtonList.push(
        <ActionButton
          key={ i }
          action={ a }
        />
      )
    }

    return (
      <div className="col-12 DiceActions">
        <h5>
          Ações de Combate
        </h5>
        <div className="boxActions">
          <strong>
            { `Ataques (${this.props.currentCreature.attacks_per_turn} / turno)` }
          </strong>
          <div className="ActionList">
            { actionButtonList }
          </div>
        </div>

        <h5>Modificadores de Ação</h5>
        <div className="boxActions">
          <div className="ActionList">
            <label>
              <input type="checkbox" id="test1" />
              Movimentação
              <strong>
                (1d6)
              </strong>
            </label>
            <label>
              <input type="checkbox" />
              Ação Bônus
              <strong>
                (1d6)
              </strong>
            </label>
          </div>
        </div>

      </div>
    );
  }
})

const ActionButton = React.createClass({
  render () {
    return (
      <label>
        <input type="checkbox" id="test1" />
        { this.props.action.name }
        <strong>
          { `(${this.props.action.damage_dice})` }
        </strong>
      </label>
    )
  }
})
