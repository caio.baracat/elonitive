const SelectCreature = React.createClass({
  render () {
    const creatureRows = [];
    for (const [i, c] of this.props.creatures.entries()) {
      creatureRows.push(
        <CreatureNavRow
          key={ i }
          creature={ c }
          onClick={ () => this.props.onSelectCreature(i) }
        />
      )
    }

    return (
      <div className="col-12 SelectCreature">
        <nav className="navbar navbar-inverse bg-inverse">
          <a className="navbar-brand" href="#" data-toggle="collapse" data-target="#creatureList">
            { this.props.currentCreature.name }
          </a>

          <div className="collapse navbar-collapse" id="creatureList">
            <ul className="navbar-nav mr-auto">
              { creatureRows }
            </ul>
          </div>
        </nav>
      </div>
    );
  }
})

const CreatureNavRow = React.createClass({
  onClick (e) {
    e.preventDefault()
    this.props.onClick()
  },
  render () {
    return (
      <li className={ "nav-item" + (this.props.creature.currentCreature ? ' active' : '') }>
        <a
          className="nav-link"
          href="#"
          data-toggle="collapse"
          data-target="#creatureList"
          onClick={ (e) => this.onClick(e) }
        >
          { this.props.creature.name }
        </a>
      </li>
    )
  }
})
