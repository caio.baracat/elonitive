const Initiative = React.createClass({
  // Initial state
  getInitialState () {
    const defaultCreatures = this.formatCreatures(this.props.creatures)
    const defaultActions = []

    return {
      creatures: defaultCreatures,
      actions: defaultActions
    }
  },

  // Make AJAX requests
  componentDidMount() {
    this.loadActions(this.state.creatures[0].id)
  },

  // Formats creatures Map to future using
  formatCreatures (creatures, currentCreatureIndex = 0) {
    const newCreatures = []

    for (const [i, c] of creatures.entries()) {
      c.currentCreature = (currentCreatureIndex == i) ? true : false
      newCreatures.push(c)
    }

    return newCreatures
  },

  // Returns current creature
  getCurrentCreature () {
    for (const c of this.state.creatures) {
      if (c.currentCreature) {
        return c
      }
    }
  },

  // Sets current creature
  setCurrentCreature (creatureIndex) {
    const newCreatures = this.formatCreatures(this.state.creatures, creatureIndex)

    this.setState({
      creatures: newCreatures,
      actions: this.state.actions
    })

    this.loadActions(this.state.creatures[creatureIndex].id)
  },

  // Loads actions to be selected
  loadActions (creatureId) {
    $.get(`/api/v1/creatures/${creatureId}/weapons.json`, (data) => {
      this.setState({
        creatures: this.state.creatures,
        actions: data
      })
    })
  },

  // Render everything
  render () {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-md-7">
            { this.getCurrentCreature().name }
          </div>
          <div className="col-12 col-md-5 sideBar">
            <div className="row">
              <SelectCreature
                creatures={ this.state.creatures }
                currentCreature={ this.getCurrentCreature() }
                onSelectCreature={ creatureId => this.setCurrentCreature(creatureId) } />
              <DiceActions
                currentCreature={ this.getCurrentCreature() }
                actions={ this.state.actions } />
            </div>
          </div>
        </div>
      </div>
    );
  }
});
