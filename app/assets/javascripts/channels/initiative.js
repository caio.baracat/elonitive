App.initiative = App.cable.subscriptions.create("InitiativeChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
    console.log("connected");
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
    console.log("disconnected");
  },

  received: function(data) {
    // Called when there's incoming data on the websocket for this channel
    console.log(data);
    return true;
  },

  add_dice: function() {
    return this.perform('add_dice');
  }
});
