class CreateCreatures < ActiveRecord::Migration[5.1]
  def change
    create_table :creatures do |t|
      t.string :name
      t.integer :dexterity
      t.integer :dex_mod
      t.integer :attacks_per_turn

      t.timestamps
    end
  end
end
