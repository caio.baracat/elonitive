class CreateAttacks < ActiveRecord::Migration[5.1]
  def change
    create_table :attacks do |t|
      t.belongs_to :creature, index: true
      t.belongs_to :weapon, index: true

      t.timestamps
    end
  end
end
