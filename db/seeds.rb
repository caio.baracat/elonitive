# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Armas Simples Corpo-a-Corpo
Weapon.create(name: "Adaga", damage_dice: "1d4")
Weapon.create(name: "Azagaia", damage_dice: "1d6")
Weapon.create(name: "Bordão", damage_dice: "1d6")
Weapon.create(name: "Clava Grande", damage_dice: "1d8")
Weapon.create(name: "Foice Curta", damage_dice: "1d4")
Weapon.create(name: "Lança", damage_dice: "1d6")
Weapon.create(name: "Maça", damage_dice: "1d6")
Weapon.create(name: "Machadinha", damage_dice: "1d6")
Weapon.create(name: "Martelo Leve", damage_dice: "1d4")
Weapon.create(name: "Porrete", damage_dice: "1d4")

# Armas Simples à Distância
Weapon.create(name: "Arco Curto", damage_dice: "1d6")
Weapon.create(name: "Besta Leve", damage_dice: "1d8")
Weapon.create(name: "Dardo", damage_dice: "1d4")
Weapon.create(name: "Funda", damage_dice: "1d4")

# Armas Marciais Corpo-a-Corpo
Weapon.create(name: "Alabarda", damage_dice: "1d10")
Weapon.create(name: "Chicote", damage_dice: "1d4")
Weapon.create(name: "Cimitarra", damage_dice: "1d6")
Weapon.create(name: "Espada Curta", damage_dice: "1d6")
Weapon.create(name: "Espada Grande", damage_dice: "2d6")
Weapon.create(name: "Espada Longa", damage_dice: "1d8")
Weapon.create(name: "Glaive", damage_dice: "1d10")
Weapon.create(name: "Lança de Montaria", damage_dice: "1d12")
Weapon.create(name: "Lança Longa", damage_dice: "1d10")
Weapon.create(name: "Maça Estrela", damage_dice: "1d8")
Weapon.create(name: "Machado de Batalha", damage_dice: "1d8")
Weapon.create(name: "Machado Grande", damage_dice: "1d12")
Weapon.create(name: "Malho", damage_dice: "2d6")
Weapon.create(name: "Mangual", damage_dice: "1d8")
Weapon.create(name: "Martelo de Guerra", damage_dice: "1d8")
Weapon.create(name: "Picareta de Guerra", damage_dice: "1d8")
Weapon.create(name: "Rapieira", damage_dice: "1d8")
Weapon.create(name: "Tridente", damage_dice: "1d6")

# Armas Marciais à Distância
Weapon.create(name: "Arco Longo", damage_dice: "1d8")
Weapon.create(name: "Besta de Mão", damage_dice: "1d6")
Weapon.create(name: "Besta Pesada", damage_dice: "1d10")
Weapon.create(name: "Rede", damage_dice: "--")
Weapon.create(name: "Zarabatana", damage_dice: "1")

# PCs
bohrn = Creature.create(
  name: "Bohrn",
  dexterity: 8,
  dex_mod: Creature.calculate_dex_mod(8),
  attacks_per_turn: 1
)

hidekunn = Creature.create(
  name: "Hidekunn",
  dexterity: 17,
  dex_mod: Creature.calculate_dex_mod(17),
  attacks_per_turn: 2
)

khaoks = Creature.create(
  name: "Khaoks",
  dexterity: 15,
  dex_mod: Creature.calculate_dex_mod(15),
  attacks_per_turn: 1
)

miau = Creature.create(
  name: "Miau",
  dexterity: 16,
  dex_mod: Creature.calculate_dex_mod(16),
  attacks_per_turn: 1
)

nohrmao = Creature.create(
  name: "Nohr'Mao",
  dexterity: 16,
  dex_mod: Creature.calculate_dex_mod(16),
  attacks_per_turn: 1
)

ozurin = Creature.create(
  name: "Ozurin Kheon",
  dexterity: 14,
  dex_mod: Creature.calculate_dex_mod(14),
  attacks_per_turn: 2
)

panda = Creature.create(
  name: "Panda Grylls",
  dexterity: 16,
  dex_mod: Creature.calculate_dex_mod(16),
  attacks_per_turn: 1
)

tolin = Creature.create(
  name: "Tolin Ovak",
  dexterity: 10,
  dex_mod: Creature.calculate_dex_mod(10),
  attacks_per_turn: 1
)

# Adicionando algumas Armas
bohrn.weapons << Weapon.find_by_name("Martelo de Guerra")
hidekunn.weapons << Weapon.find_by_name("Bordão")
khaoks.weapons << Weapon.find_by_name("Besta Leve")
nohrmao.weapons << Weapon.find_by_name("Arco Longo")
ozurin.weapons << Weapon.find_by_name("Machadinha")
panda.weapons << Weapon.find_by_name("Espada Longa")
tolin.weapons << Weapon.find_by_name("Espada Longa")
